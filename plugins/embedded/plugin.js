/**
 * @file
 * Define tags as "block level" so the editor doesn't try to put paragraph tags around them.
 */

cktags = ['cktimeline'];

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

CKEDITOR.plugins.add('embedded', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor) {
    // Define plugin name.
    var pluginName = 'embedded';

    // Set up object to share variables amongst scripts.
    CKEDITOR.embedded = {};

    // Register button for Timeline.
    editor.ui.addButton('Timeline', {
      label : "Add/Edit Timeline",
      command : 'timeline',
      icon: this.path + 'icons/timeline.png',
    });
    // Register right-click menu item for Timeline.
    editor.addMenuItems({
      timeline : {
        label : "Edit Timeline",
        icon: this.path + 'icons/timeline.png',
        command : 'timeline',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.cktimeline = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.cktimeline = 1;
    CKEDITOR.dtd.body.cktimeline = 1;
    // Make cktimeline to be a self-closing tag.
    CKEDITOR.dtd.$empty.cktimeline = 1;

    // Make sure the fake element for Timeline has a name.
    CKEDITOR.embedded.cktimeline = 'Timeline';
    CKEDITOR.lang.en.fakeobjects.cktimeline = CKEDITOR.embedded.cktimeline;
    // Add JavaScript file that defines the dialog box for Timeline.
    CKEDITOR.dialog.add('timeline', this.path + 'dialogs/timeline.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('timeline', new CKEDITOR.dialogCommand('timeline'));

    // Regular expressions for timeline.
    CKEDITOR.embedded.timeline_restfulurl_regex = /^[1-9][0-9]*$/;

    // Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'cktimeline') {
        evt.data.dialog = 'timeline';
      }
    });

    // Add the appropriate right-click menu item if an element is right-clicked.
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element, selection) {
        if (element && element.is('img') && element.data('cke-real-element-type') === 'cktimeline') {
          return { timeline : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    // Add CSS to use in-editor to style custom (fake) elements.
    CKEDITOR.addCss(
      'img.cktimeline {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/timeline.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 50px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      // Timeline: Definitions for wide width.
      '.uw_tf_standard_wide p img.cktimeline {' +
        'height: 50px;' +
      '}' +

      // Timeline: Definitions for standard width.
      '.uw_tf_standard p img.cktimeline {' +
        'height: 50px;' +
      '}'
    );
  },
  afterInit : function (editor) {
    // Make fake image display on first load/return from source view.
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          cktimeline : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cktimeline = CKEDITOR.embedded.cktimeline;
            // Adjust title if a list name is present.
            if (element.attributes['data-restfulurl'] || element.attributes['data-timeline-nid']) {
              CKEDITOR.lang.en.fakeobjects.cktimeline += ': ' + element.attributes['data-timeline-nid'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'cktimeline', 'cktimeline', false);
          }
        }
      });
    }
  }
});
