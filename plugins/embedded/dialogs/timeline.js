/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var timelineDialog = function (editor) {
    return {
      title : 'Timeline Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'timeline',
        label: 'timeline',
        elements:[{
          type: 'text',
          id: 'timelineInput',
          label: 'Please enter embedded timeline node ID:',
          required: true,
          setup: function (element) {
                   this.setValue(element.getAttribute('data-timeline-nid'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        timelineInput = this.getValueOf('timeline','timelineInput');

        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";

        if (!CKEDITOR.embedded.timeline_restfulurl_regex.test(timelineInput)) {
            errors += "You must enter a node ID.\r\n";
        }
        else {
          errors = '';
        }
        if (!timelineInput) {
          errors = "You must enter a node ID.\r\n";
        }

        if (errors == '') {
          // Variable used to store number of embedded timeline (call to php function).
          var num_of_timeline;

          // Use jQuery ajax call to check if nid supplied is a embedded timeline.
          jQuery(function ($) {

            // Make the ajax call, set to synchronous so that we wait to get a response.
            $.ajax({
              type: 'get',
              async: false,
              url: Drupal.settings.basePath + "ajax/nid_exists/" + timelineInput + "/uw_embedded_timeline",

              // If we have success set to the global variable, so that we can use it later to check for errors.
              success: function (data) {
                num_of_timeline = data;
              }
            });
          });

          // If the number of embedded timeline is 0 (meaning there are no embedded timeline with that nid), set the errors.
          if (num_of_timeline == 0) {
            errors = 'You must enter a valid embedded timeline node ID.\r\n';
          }
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the timeline element.
          var cktimelineNode = new CKEDITOR.dom.element('cktimeline');
          // Save contents of dialog as attributes of the element.
          cktimelineNode.setAttribute('data-timeline-nid', timelineInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.cktimeline = CKEDITOR.socialmedia.cktimeline + ': ' + timelineInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(cktimelineNode, 'cktimeline', 'cktimeline', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('cktimeline');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.cktimeline = CKEDITOR.socialmedia.cktimeline;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.cktimelineNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = fakeImage;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cktimeline') {
          this.fakeImage = fakeImage;
          var cktimelineNode = editor.restoreRealElement(fakeImage);
          this.cktimelineNode = cktimelineNode;
          this.setupContent(cktimelineNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('timeline', function (editor) {
    return timelineDialog(editor);
  });
})();
