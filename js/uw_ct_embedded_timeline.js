/**
 * @file
 */
(function ($) {
  Drupal.behaviors.uw_ct_embedded_timeline = {
    attach: function (context, settings) {

      $(window).load(function() {

        // Step through each next and move to the left
        // to account for the scroll bar.
        $('.vco-feature .nav-next').each(function (index, value) {
          if ($('body.wide').length == 0) {
            $(this).css('left', '374px');
          }
          else {
            $(this).css('left', '625px');
          }
        });

        // Step through each previous and move to the right
        // to match the move of the next.
        $('.vco-feature .nav-previous').each(function (index, value) {
          if ($('body.wide').length == 0) {
            $(this).css('left', '22px');
          }
          else {
            $(this).css('left', '45px');
          }
        });

      });
    }
  };
})(jQuery);