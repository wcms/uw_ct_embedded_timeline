<?php
/**
 * @file
 * uw_ct_embedded_timeline.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_embedded_timeline_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_embedded_timeline content'.
  $permissions['create uw_embedded_timeline content'] = array(
    'name' => 'create uw_embedded_timeline content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_embedded_timeline content'.
  $permissions['delete any uw_embedded_timeline content'] = array(
    'name' => 'delete any uw_embedded_timeline content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_embedded_timeline content'.
  $permissions['delete own uw_embedded_timeline content'] = array(
    'name' => 'delete own uw_embedded_timeline content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_embedded_timeline content'.
  $permissions['edit any uw_embedded_timeline content'] = array(
    'name' => 'edit any uw_embedded_timeline content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_embedded_timeline content'.
  $permissions['edit own uw_embedded_timeline content'] = array(
    'name' => 'edit own uw_embedded_timeline content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_embedded_timeline revision log entry'.
  $permissions['enter uw_embedded_timeline revision log entry'] = array(
    'name' => 'enter uw_embedded_timeline revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_timeline authored by option'.
  $permissions['override uw_embedded_timeline authored by option'] = array(
    'name' => 'override uw_embedded_timeline authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_timeline authored on option'.
  $permissions['override uw_embedded_timeline authored on option'] = array(
    'name' => 'override uw_embedded_timeline authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_timeline comment setting option'.
  $permissions['override uw_embedded_timeline comment setting option'] = array(
    'name' => 'override uw_embedded_timeline comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_timeline promote to front page option'.
  $permissions['override uw_embedded_timeline promote to front page option'] = array(
    'name' => 'override uw_embedded_timeline promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_timeline published option'.
  $permissions['override uw_embedded_timeline published option'] = array(
    'name' => 'override uw_embedded_timeline published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_timeline revision option'.
  $permissions['override uw_embedded_timeline revision option'] = array(
    'name' => 'override uw_embedded_timeline revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_timeline sticky option'.
  $permissions['override uw_embedded_timeline sticky option'] = array(
    'name' => 'override uw_embedded_timeline sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_embedded_timeline content'.
  $permissions['search uw_embedded_timeline content'] = array(
    'name' => 'search uw_embedded_timeline content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
